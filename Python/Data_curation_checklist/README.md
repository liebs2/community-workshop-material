![PANGAEA Logo](../../banner.png)

#### PANGAEA Data Curation Checklist
Version: 1.1<br>
By: Michael Oellermann, Kathrin Riemann-Campe<br>
Last updated: 2023-10-30

This notebook aims to help you, to check and resolve some common problems and issues with data tables, before submitting them to a data repository like [PANGAEA](https://www.pangaea.de/submit/).

Check out our [instructions](https://wiki.pangaea.de/wiki/Data_submission) and [data templates](https://wiki.pangaea.de/wiki/Best_practice_manuals_and_templates) for submissions to PANGAEA.

Run this notebook in:
* MyBinder [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.gwdg.de%2Fjoerg.bruenecke1%2Fgfowinterschoolmo/HEAD)
* GoogleColab <a target="_blank" href="https://colab.research.google.com/github/pangaea-data-publisher/community-workshop-material/blob/master/Python/Data_curation_checklist/Data_curation_checklist.ipynb">
  <img src="https://colab.research.google.com/assets/colab-badge.svg" alt="Open In Colab"/>
</a>

# Acknowledgements

Michael Oellermann/this work was supported by the German Research Foundation DFG under the grant agreement number  442032008 (NFDI4Biodiversity). The project is part of NFDI, the National Research Data Infrastructure Programme in Germany.

<img src="./NFDI_4_Biodiversity___Logo_Positiv.png" width="300">
